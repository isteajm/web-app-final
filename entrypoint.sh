#!/bin/bash
 
# set -e
 
# Evaluar si el comando ping existe, de lo contrario se instala.
 
 
apt update && apt install apache2 git -y
 
deploy_app () {
 
    cd /web-app-final && git pull
    cp /web-app-final/album/* /var/www/html
 
 
}
 
 
# Si el directorio '/web-app-final' no existe
# clonarlo
 
if ! test -d /web-app-final; then
 
    git clone https://gitlab.com/isteajm/web-app-final.git /web-app-final
 
    deploy_app
else
 
    # Si el repositorio ya se ha cloando
    # traer posibles cambios de la rama main
 
    deploy_app
fi
 
 
 
 
 
exec "$@"
